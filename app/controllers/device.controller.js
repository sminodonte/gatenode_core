const logger = require('../plugins/logger');
// Read do_not_touch_file 
require('dotenv').config({ path: 'do_not_touch_file' });
// Write do_not_touch_file
const updateDotenv = require('../tools/update-dotenv');

const axios = require('axios');
const settings = require("../config/settings.config.js");
const db = require("../models");
const Node = db.nodes;

// Create NODE_TEMP Device on middle-node network
exports.createInit = (req, res, next) => {
  logger.info("INIT REGISTER NODE");

  logger.info(req);
  // REGISTER DEVICE /devices to MIDDLE-NODE
  // curl -X POST -d "type=init&ip=..." "http://192.168.193.116:1080/devices"
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/',
    {
      name: req.name,
      user: "human",
      type: req.type,
      data: {},
      "meshblu": {
        "version": "2.0.0",
        "whitelists": {
          "message" : {
            "from" : []
          },
          "discover" : {
            "as" : [{"uuid" : "*"}],
            "view" : [{"uuid" : "*"}]
          }
        }
      }
    })
  .then((res) => {
    //logger.info(`statusCode: ${res.statusCode}`)
    logger.info("------> NEW DEVICE")
    //const device = new Device({});
    const node = new Node({
      uuid: res.data.uuid,
      token: res.data.token,
      name: req.name,
      type: req.type
    });

    // Save new Device in the database
    node
      .save(node)
      .then(data => {
        if(req.type==='NODE_TEMP'){
          updateDotenv({
            GATE_NODE_DEVICE_INIT_UUID: res.data.uuid,
            GATE_NODE_DEVICE_INIT_TOKEN: res.data.token
          }, '../../do_not_touch_file').then((newEnv) => logger.info('do_not_touch_file saved!', newEnv))
        }       
        res.send(data);
      })
      .catch(err => {
        //res.status(500).send({
        //  message:
        err || "Some error occurred while creating the Device."
        //});
      });
  })
  .catch((error) => {
    console.error(error)
  })
};

// Create NODE_TEMP Device on middle-node network
exports.testRegister = (req, res, next) => {
  logger.info("TEST REGISTER NODE");

  logger.info(req);
  // REGISTER DEVICE /devices to MIDDLE-NODE
  // curl -X POST -d "type=init&ip=..." "http://192.168.193.116:1080/devices"
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/',
    {
      name: req.name,
      user: "robot",
      type: req.type,
      data: {},
      "meshblu": {
        "version": "2.0.0",
        "whitelists": {
          "message" : {
            "from" : []
          },
          "discover" : {
            "as" : [{"uuid" : "*"}],
            "view" : [{"uuid" : "*"}]
          }
        }
      }
    })
  .then((res) => {
    //logger.info(`statusCode: ${res.statusCode}`)
    logger.info("------> TEST REGISTER DEVICE")
    logger.info(res.data);
  })
  .catch((error) => {
    console.error(error)
  })
};

// Create new Device on middle-node network
exports.register = (req,res,next) => {
  var _id;
  logger.info("REGISTER NODE");
  var body =JSON.parse(req.body);

  const name = "INIT";
  var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  logger.info("1) SEARCH IF EXIST ALREADY A NODE WITH THE SAME NAME IN LOCAL DB");  
  Node.find(condition)
    .then(data => {
      if(data.length!=0){
        logger.info("FIND INIT NODE");
        _id = data[0]._id;
        axios.defaults.headers = {
          'meshblu_auth_uuid': data[0].uuid,
          'meshblu_auth_token': data[0].token
        }
        logger.info("2) SEARCH IF EXIST ALREADY A NODE WITH THE SAME NAME IN MIDDLE-NODE");
        // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
        axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices?name='+body.name)
        .then(function (response) {
          logger.info(response.data);
          if(response.data.devices.length!=0){
            logger.info("This node already exists!");
            //next(res.status(200).json({ message: 'the system is not initialized!' }));
            next(res.status(500).send({
              message: `Please try again with another name.`,
              headertext: `This node already exists!`
            }));
           
            //res.status(200).json({ message: 'This name already exists!' });
            // logger.info(response.data);
            // logger.info(response.status);
            // logger.info(response.statusText);
  
          }
          else{ 
            logger.info("3) IF IS NOT EXIST ALREADY A NODE WITH TEH SAME NAME CREATE ONE");

            // curl -X POST -d "type=init&ip=..." "http://192.168.193.116:1080/devices"
            axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/',
            {
              name: body.name,
              user: "robot",
              type: body.type,
              data: {},
              public_ip: process.env.PUBLIC_IP,
              local_ip: process.env.LOCAL_IP,
              local_vpn_ip: process.env.LOCAL_VPN_IP,
              "meshblu": {
                "version": "2.0.0",
                "whitelists": {
                  "message" : {
                    "from" : []
                  },
                  "discover" : {
                    "as" : [{"uuid" : "*"}],
                    "view" : [{"uuid" : "*"}]
                  }
                }
              }
            })
            .then((res2) => {
              logger.info("4) DELETE INIT NODE ON MIDDLE-NODE");

              axios.defaults.headers = {
                'meshblu_auth_uuid': data[0].uuid,
                'meshblu_auth_token': data[0].token
              }
              logger.info(data[0].uuid)
              axios.delete('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/'+data[0].uuid, {
              })
              .then(function (response) {
                logger.info("Node deleted:");
                logger.info(response.data);
              })
              .catch(function (error) {
                logger.info(error);
              })
              .then(function () {
                // always executed
              });
               
              logger.info("------> NEW DEVICE")

              const node = new Node({
                uuid: res2.data.uuid,
                token: res2.data.token,
                name: body.name,
                type: body.type
              });
              logger.info("5) CREATE A NEW NODE ON LOCAL DB");
              node
                .save(node)
                .then(data => {
                  logger.info("data");
                  logger.info(data);

                  updateDotenv({
                    GATE_NODE_DEVICE_UUID: res2.data.uuid,
                    GATE_NODE_DEVICE_TOKEN: res2.data.token,
                    GATE_NODE_INIT: 'true'
                  }, '../../do_not_touch_file').then((newEnv) => {
                    logger.info('do_not_touch_file saved!', newEnv);
                     // INIT MIDDLE-NODE SOCKETIO 
                    //middlenode.connect();
                  })
               
                  //res2.send(data);
                  //next(res.send(data));
                })
                .catch(err => {
                  logger.info(err);
                  // next(res2.status(500).send({
                  //   message: err.message || `Some error occurred while creating the Node on local DB.`, 
                  //   headertext: `Error!`
                  // }));
                });

                logger.info("6) DELETE INIT NODE  ON LOCAL DB");
                Node.findByIdAndRemove(_id, { useFindAndModify: false })
                  .then(data => {
                    if (!data) {
                      next(res.status(404).send({
                        message: `Cannot delete Node with id=${_id}. Maybe Node was not found!`, 
                        headertext: `Error!`
                      }));
                    } else {
                      logger.info("Node was deleted successfully then node was registered successfully!");
                      next(res.send({
                        message: `Node was registered successfully!`, 
                        headertext: `Success!`
                      }));
                    }
                  })
                  .catch(err => {
                    // logger.info(err);
                    // next(res.status(500).send({
                    //   message:  err.message || `Could not delete Node with id=${_id}.`, 
                    //   headertext: `Error!`
                    // }));
                  });
            })
            .catch((err) => {
              logger.info(err);
              next(res.status(500).send({
                message: err.message || `Some error occurred while searching node in Middle-Node.`, 
                headertext: `Error!`
              }));
            })

          }
        })
        .catch(function (error) {
          //logger.info(error);
        })
        .then(function () {
          // always executed
        });  


      }else{
        logger.info("NOT FIND INIT NODE");
        next(res.status(500).send({
          message: `Please use docker-compose down and after docker-compose up to reboot the system.`,
          headertext: `The system is not initialized!`
        }));
        // res.status(200).json({ message: 'the system is not initialized!' });  
      }
      //res.send(data);
    })
    .catch(err => {
      //logger.info(err)
      next(res.status(500).send({
          message: err.message || `Some error occurred while retrieving INIT node.`,
          headertext: `Error!`
        }));   
    });

};

// Returns information about the currently authenticated device
// /v2/whoami 
exports.whoami = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
  }
  // axios.defaults.headers = {
  //   'meshblu_auth_uuid': req.query.uuid,
  //   'meshblu_auth_token': req.query.token
  // }

  // logger.info(req.query)
  // Returns the Middle-Node platform status
  // curl -X GET "http://192.168.193.116:1080/v2/whoami
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/whoami', {
  })
  .then(function (response) {    
    response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    delete response.data.meshblu //delete object with old key name
    logger.info("Whoami data:");
    logger.info(response.data)
    res.json(response.data) 
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  

};
// Retrieve all Devices UUUIDs from middle-node network
exports.status = (req, res, next) => {

  // Returns the Middle-Node platform status
  // curl -X GET "http://192.168.193.116:1080/status
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/status', {
  })
  .then(function (response) {    
    response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    delete response.data.meshblu //delete object with old key name
    logger.info("STATUS MIDDLE-NODE:");
    logger.info(JSON.stringify(response.data))
    //res.json(response.data) 
    next(response.data)

  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });    

};

// Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
exports.sendMessages = (req, res, next) => {

  logger.info("sendMessages");
  logger.info(req.uuid)
  logger.info(req.message)
  logger.info(JSON.stringify(req.message))

  axios.defaults.headers = {
    'Content-Type': 'application/json',
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/messages', JSON.stringify(req.message))
  .then(function (response) {

    logger.info(response.data);
    logger.info("Sent Message from "+req.uuid+" to ");
    //res.json(response.data) 
    next(response.data)
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
exports.messages = (req, res, next) => {
  logger.info("messages");

  axios.defaults.headers = {
    'Content-Type': 'application/json',
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
    }
  //logger.info(req.message)

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/messages', JSON.parse(req.body).message)
  .then(function (response) {
    logger.info(response.data);
    res.json(response.data) 
    //next(response.data)
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// GET ALL SUBSCRIPTIONS
// Get a list of server-side subscriptions from one device to another.
// get /v2/devices/:subscriberUuid/subscriptions
// curl -X GET "http://127.0.0.1:1080/v2/devices/bd59361e-f438-47a3-873a-d0bf0dc9fedc/subscriptions" --header "meshblu_auth_uuid: bd59361e-f438-47a3-873a-d0bf0dc9fedc" --header "meshblu_auth_token: fe7083fdf6f1f870add449bb5d80d39b148e306d"
exports.subscriptionsList = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
    }

  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+JSON.parse(req.body).uuid+'/subscriptions')
  .then(function (response) {
    logger.info(response.data);
    res.json(response.data) 
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Create a server-side subscription from one device to another.
// post /v2/devices/:subscriberUuid/subscriptions/:emitterUuid/:type 
exports.subscription = (req, res, next) => {

  axios.defaults.headers = {
    'Content-Type': 'application/json',
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
  }

  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+JSON.parse(req.body).subscriberUuid+'/subscriptions/'+JSON.parse(req.body).emitterUuid+'/'+JSON.parse(req.body).type,{})
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Remove a server-side subscription from one device to another.
// delete /v2/devices/:subscriberUuid/subscriptions/:emitterUuid/:type 
exports.deleteSubscription = (req, res, next) => {
  //const id = req.params.id;
  axios.defaults.headers = {
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
  }

  logger.info(JSON.parse(req.body))
  // IN THE CASE THE EMITTER AND THE SUBSCRIBER ARE THE SAME
  axios.delete('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+JSON.parse(req.body).subscriberUuid+'/subscriptions/'+JSON.parse(req.body).emitterUuid+'/'+JSON.parse(req.body).type,{})
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });
  
};

// Returns an array of device UUIDs based on key/value query criteria
exports.findAllArray = (req, res, next) => {
  // axios.defaults.headers = {
  //   'meshblu_auth_uuid': req.headers.uuid,
  //   'meshblu_auth_token': req.headers.token
  //   }
  //   var key = {
  //     'value': 'type='+req.params
  //   }

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.query.uuid,
    'meshblu_auth_token': req.query.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices')
  .then(function (response) {
 
    for (var i = 0; i < response.data.length; i++) {
      response.data[i].middlenode = response.data[i].meshblu;
      delete response.data[i].meshblu;
    }

    // response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    // delete response.data.meshblu
    // logger.info(response.data);
    // if (!response.data) {
    //   next(response.status(404).send({
    //     message: `Nodes were not found!`, 
    //     headertext: `Error!`
    //   }));
    // } else {
              //res.send(response);
      res.json(response.data)
      logger.info(response.data);
      logger.info("This is node's list!");
      // next(response.send({
      //   message: `Node list!`, 
      //   headertext: `Success!`
      // }));
    // }
  })
  .catch(function (error) {
    logger.info(error);
    next(res.status(500).send({
      message: error.message || `Some error occurred while searching node in Middle-Node.`, 
      headertext: `Error!`
    }));
  })
  .then(function () {
    // always executed
  });  
};

// Returns an array of device UUIDs based on key/value query criteria
exports.findByKey = (req, res, next) => {
  // axios.defaults.headers = {
  //   'meshblu_auth_uuid': req.headers.uuid,
  //   'meshblu_auth_token': req.headers.token
  //   }
  //   var key = {
  //     'value': 'type='+req.params
  //   }
  
  axios.defaults.headers = {
    'meshblu_auth_uuid': req.query.uuid,
    'meshblu_auth_token': req.query.token
    }
  logger.info("Find by Key:")


  var key=Object.keys(req.query.data)
  var value=Object.values(req.query.data)

  var query = '';
  for(var i=0; i<key.length; i++){
    query += key[i]+'='+value[i]+'&'
  }
  logger.info("Query:");
  logger.info(query)

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices?'+query)
  .then(function (response) {
   console.log(response.data)
    for (var i = 0; i < response.data.length; i++) {
      response.data[i].middlenode = response.data[i].meshblu;
      delete response.data[i].meshblu;
    }

    // response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    // delete response.data.meshblu
    // logger.info(response.data);
    // if (!response.data) {
    //   next(response.status(404).send({
    //     message: `Nodes were not found!`, 
    //     headertext: `Error!`
    //   }));
    // } else {
              //res.send(response);
      
      logger.info(response.data);
      logger.info("This is node's list!");
      next(response.data)
      // next(response.send({
      //   message: `Node list!`, 
      //   headertext: `Success!`
      // }));
    // }
  })
  .catch(function (error) {
    logger.info(error);
    // next(res.status(500).send({
    //   message: error.message || `Some error occurred while searching node in Middle-Node.`, 
    //   headertext: `Error!`
    // }));
  })
  .then(function () {
    // always executed
  });  
};

// Returns an array of device UUIDs based on key/value query criteria
exports.findByName = (req, res, next) => {
  // axios.defaults.headers = {
  //   'meshblu_auth_uuid': req.headers.uuid,
  //   'meshblu_auth_token': req.headers.token
  //   }
  //   var key = {
  //     'value': 'type='+req.params
  //   }
  
  axios.defaults.headers = {
    'meshblu_auth_uuid': req.query.uuid,
    'meshblu_auth_token': req.query.token
    }
    // logger.info(req.query.uuid)
    // logger.info(req.query)
    // logger.info(JSON.stringify(req.uuid))


  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices?name='+req.query.name)
  .then(function (response) {
   console.log(response.data)
    for (var i = 0; i < response.data.length; i++) {
      response.data[i].middlenode = response.data[i].meshblu;
      delete response.data[i].meshblu;
    }

    // response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    // delete response.data.meshblu
    // logger.info(response.data);
    // if (!response.data) {
    //   next(response.status(404).send({
    //     message: `Nodes were not found!`, 
    //     headertext: `Error!`
    //   }));
    // } else {
              //res.send(response);
      
      logger.info(response.data);
      logger.info("This is node's list!");
      next(response.data)
      // next(response.send({
      //   message: `Node list!`, 
      //   headertext: `Success!`
      // }));
    // }
  })
  .catch(function (error) {
    logger.info(error);
    // next(res.status(500).send({
    //   message: error.message || `Some error occurred while searching node in Middle-Node.`, 
    //   headertext: `Error!`
    // }));
  })
  .then(function () {
    // always executed
  });  
};

// Retrieve all Nodes from the database.
exports.findAll = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/search/devices', {
    type: req.type

  })
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Find a single Node with an id
exports.findOne = (req, res, next) => {
  //const id = req.params.id;

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }
    
  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/'+req.uuid, {
  })
  .then(function (response) {
    response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    delete response.data.meshblu
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Find a single Node with an id
exports.findOneV2 = (req, res, next) => {
  //const id = req.params.id;

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }
  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+req.uuid, {
  })
  .then(function (response) {
    response.data.middlenode = response.data.meshblu // on object create new key name. Assign old value to this
    delete response.data.meshblu
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Returns all information (including tokens) of all devices or nodes belonging to a user's UUID (identified with an "owner" property and user's UUID i.e. "owner":"0d1234a0-1234-11e3-b09c-1234e847b2cc")
exports.findMyDevices = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/mydevices/', {
  })
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};

// Update a Node by the id in the request
exports.update = (req, res, next) => {
  //const id = req.params.id;

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.put('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/'+req.uuid, {
    type: req.type
  })
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
    // 
  });  
};

// Update a Node by the id in the request
// Updates a node or device currently registered with Middle-Node that you have access to update. You can pass any key/value pairs to update object. PUT expects a complete representation of the device, so all omitted keys will be removed on save with the exception of the UUID. Allows the use of $set, $inc, $push, etc. operators as documented in the MongoDB API
exports.updateV2_PUT = (req, res, next) => {
  //const id = req.params.id;
logger.info("DEVICE updateV2_PUT");
  // axios.defaults.headers = {
  //   'meshblu_auth_uuid': req.headers.uuid,
  //   'meshblu_auth_token': req.headers.token
  //   }

  // logger.info(req.headers.uuid);
   axios.defaults.headers = {
    'meshblu_auth_uuid': JSON.parse(req.body).uuid,
    'meshblu_auth_token': JSON.parse(req.body).token
    }
 logger.info(req.body)


  logger.info(JSON.parse(req.body).data);
  logger.info(JSON.parse(req.body).uuid);

  axios.put('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+JSON.parse(req.body).uuid, JSON.parse(req.body).data) //+req.uuid, req.data)
  .then(function (response) {
    logger.info(response.data);
    res.json(response.data)
    logger.info("The node was updated successfully!");
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
    // 
  });  
};

// Update a Node by the id in the request
// Updates a node or device currently registered with Middle-Node that you have access to update. You can pass any key/value pairs to update object. This does a diff only (Equivalent to using $set and PUT)
exports.updateV2_PATCH = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.patch('http://'+"locahost"+':'+process.env.MIDDLENODE_HTTP_PORT+'/v2/devices/'+req.uuid, req.data)
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
    // 
  });  
};

// Delete a Node with the specified id in the request
exports.delete = (req, res, next) => {
  //const id = req.params.id;
  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }
  logger.info(req.uuid);
  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.delete('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/'+req.uuid, {
  })
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });
  
};

// Retrieve all Nodes from the database.
exports.resetToken = (req, res, next) => {

  axios.defaults.headers = {
    'meshblu_auth_uuid': req.uuid,
    'meshblu_auth_token': req.token
    }

  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices/'+req.uuid+'/token', {
  })
  .then(function (response) {
    logger.info(response.data);
  })
  .catch(function (error) {
    logger.info(error);
  })
  .then(function () {
    // always executed
  });  
};




// Create and Save a new Node
// exports.register = (req, res) => {
//   // Validate request
//   if (!req.body.name) {
//     res.status(400).send({ message: "Content can not be empty!" });
//     return;
//   }
//   // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
//   // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
//   axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices', {
//     params: {
//       name: req.body.name
//     }
//   })
//   .then(function (response) {
//     logger.info(response);
//   })
//   .catch(function (error) {
//     logger.info(error);
//   })
//   .then(function () {
//     // always executed
//   });  

//   // CERCARE NODO CON LO STESSO NOME
//   // SE NON SI TROVA REGISTRO NUOVO NODO
//   //   POI CERCO NODO MANAGER ED ESEGUO UN UPDATE DEL NODO CON LE NUOVE INFORMAZIONI
//   // DIVERSAMENTE INVIO NOTIFICA "ESISTE GIà QUESTO NOME"
  
//   // Create a Node
//   const node = new Node({
//     title: req.body.title,
//     description: req.body.description,
//     published: req.body.published ? req.body.published : false
//   });

//   // Save Node in the database
//   node
//     .save(node)
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while creating the Node."
//       });
//     });
// };
// // Delete all Nodes from the database.
// exports.deleteAll = (req, res) => {
//   Node.deleteMany({})
//     .then(data => {
//       res.send({
//         message: `${data.deletedCount} Nodes were deleted successfully!`
//       });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while removing all Nodes."
//       });
//     });
// };

// // Find all published Nodes
// exports.findAllPublished = (req, res) => {
//   Node.find({ published: true })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving Nodes."
//       });
//     });
// };