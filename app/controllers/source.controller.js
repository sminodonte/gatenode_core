const logger = require('../plugins/logger');
const sources = require("../config/sources.config.js");

const db = require("../models");
const Source = db.sources;


// Retrieve all Sources from the database.
exports.findAll = (req, res) => {

  res.json(sources)

  // const title = req.query.title;
  // var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  // Source.find(condition)
  //   .then(data => {
  //     res.send(data);
  //   })
  //   .catch(err => {
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while retrieving Sources."
  //     });
  //   });

};