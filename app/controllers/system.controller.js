const logger = require('../plugins/logger');
const devices = require("./device.controller.js");
const Joi = require('joi'); 


// Start data acquisition from algorithms for one or more source streams.
exports.start = (req, res) => {


  // const schemaRules ={ 
  //   algorithms: Joi.array().items(Joi.string().required()),
  //   sources: Joi.array().items(Joi.string().required()), 
  // };


  // // create schema object with rules
  //   const schema = Joi.object(schemaRules);
  //   // schema options
  //   const options = {
  //       abortEarly: false, // include all errors
  //       allowUnknown: true, // ignore unknown props
  //       stripUnknown: true // remove unknown props
  //       //stripUnknown: { arrays: true }

  //   };

  //   // validate request body against schema
  //   const { error, value } = schema.validate(req.body, options);
    
  //   if (error) {
  //       // on fail return comma separated errors
  //       next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
  //   } else {
  //       // on success replace req.body with validated value and trigger next middleware function
  //       req.body = value;
  //       next();
  //   }



var body =JSON.parse(req.body)
// console.log(body.algorithms)
//   if (!body.algorithms) {
//     return res.status(400).send({
//       message: "Algorithms and Sources fields can not be empty!"
//     });
//   }

	logger.info("START EXECUTION");
	logger.info(req.body);
	
	//logger.info(body.algorithms);
	res.json("The algorithms are starting up!")

	// {"algorithms":["crowd_counting","people_flux"],"sources":["http://video.com"],"options":["out"]}
	

	// SEARCH UUID OF NODES FROM NAME OF ALGORITHM
	body.algorithms.map(function (item) {
    logger.info("Receiver name algorithm:");
    logger.info(item);
    var query = {"algorithm":item}
    logger.info(query)
    
    logger.info("GET findByName");
    //logger.info(req)
    
    var req = {
      'query':{
        'uuid': process.env.GATE_NODE_DEVICE_UUID,
        'token': process.env.GATE_NODE_DEVICE_TOKEN,
        'data': {"algorithm":item}
      }
    } 
      
    devices.findByKey(req,res, function(result){
      logger.info("Find device:");
      logger.info(result[0].uuid);

      logger.info("MESSAGE-----------");
       logger.info(process.env.GATE_NODE_DEVICE_UUID);
        logger.info(process.env.GATE_NODE_DEVICE_TOKEN);
         logger.info(data.sender);
          logger.info(data.topic);
           logger.info(data.data.payload);

      //logger.info(JSON.stringify(JSON.parse(result[0])));

      var message={
        "uuid":process.env.GATE_NODE_DEVICE_UUID,
        "token":process.env.GATE_NODE_DEVICE_TOKEN, 
        "message":{
          "devices":[result[0].uuid],
          "sender": data.sender,
          "topic":data.topic,
          "time": Date.now(),
          "payload": {"action":"start"}
        }
      }
    
      logger.info("AFTER MESSAGE-----------");
      // Send data from GATE-NODE to MIDDLE-NODE 
      devices.sendMessages(message,res, function(result){
        logger.info("Response message:");
      });
    });
  }); 


	// SEND MESSAGE TO EVERY NODE

}

// Stop data acquisition of all algorithms for all source streams. 
exports.stop = (req, res, next) => {
	logger.info("STOP EXECUTION");
	//logger.info(req.body);
  res.json("The algorithms are stopping!")
  next();
};
