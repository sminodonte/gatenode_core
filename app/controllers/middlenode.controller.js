// SOCKETIO CONTROLLER FOR COMMUNICATION BETWEEN GATE-NODE AND MIDDLE-NODE

const logger = require('../plugins/logger');
const debug = require("debug")('socket.io-client');
const socketIoClient = require('socket.io-client');
const sse = require("./sse.controller.js");

// CREATE MIDDLE-NODE CONNECTION
exports.create = (req, res) => {
  const middlenode = socketIoClient('ws://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_SOCKETIO_PORT, {path: "/socket.io/v2"});

// console.log("socketio")
// console.log(socketio)

  // SOCKETIO CONNECT
  logger.info(process.env.GATE_NODE_DEVICE_UUID+" CONNECTING TO MIDDLE-NODE..."); 
  logger.info("FUORI")
  //console.dir(middlenode)
  middlenode.on("connect", function(){
  logger.info("DENTRO")

    // LOGIN TO MIDDLE-NODE
    middlenode.emit('identity', {
      uuid: process.env.GATE_NODE_DEVICE_UUID,
      token: process.env.GATE_NODE_DEVICE_TOKEN
    }, function(res){
      logger.info(res);
    });

    // ERROR IF THE AUTHENTICATION TO MIDDLE-NODE FAILED
    middlenode.on('notReady', function(data){
      if (data.status == 401 || data.status == 402){
        logger.error('MIDDLE-NODE SOCKETIO UUID FAILED AUTHENTICATION!');
        logger.error(JSON.stringify(data, null, 2));
        // localStorage.removeItem("uuid");
        // localStorage.removeItem("token");
      }
      if (data.status == 504){
        logger.error('SOMETHING WRONG...TIMEOUT WITH SOCKETIO');
        logger.error(data);
      }
    });

    // CONNECTION READY
    middlenode.on('ready', function(data){
      //logger.info(JSON.stringify(data));
      if (data.status == 201){
        logger.info('MIDDLE-NODE: NODE AUTHENTICATED!');
      }
      // INSERT HERE THE FUNCTION TO CALL DIFFERENT PROTOCOLS
      //protocols.socketio(middlenode);
    });

    // DISCONNECT
    middlenode.on("disconnect", function () {
      logger.info("Client MIDDLE-NODE gone")
      middlenode.removeAllListeners('middlenode');
      middlenode.removeAllListeners('disconnect');
      middlenode.removeAllListeners('connect');        
    });

    // Received data from MIDDLE-NODE to GATE-NODE    
    middlenode.on("message", function(message){
      logger.info("----------PROTOCOL SOCKETIO----------");
      logger.info("Received data from MIDDLE-NODE:");

      // if(message.)
      //   send myip

      logger.info(JSON.stringify(message));
      //var message = message;
      //CHECK IF THERE IS ATTRIBUTE ON DATA STREAM
      //
      //  
      // Sent data from GATE-NODE to DAT (Data Aquisition & Trasformation)
      gatenode.emit('gatenode', message)

      // SSE ON GUI
      app.emit("messages", message)

      // var pathImage = message.payload.url;
      // document.getElementById('image').src = pathImage;
    });


  });
};

