const logger = require('../plugins/logger');

// Read do_not_touch_file 
require('dotenv').config({ path: 'do_not_touch_file' })
// Write do_not_touch_file
const updateDotenv = require('../tools/update-dotenv');

const axios = require('axios');
const settings = require("../config/settings.config.js");
const db = require("../models");
const Node = db.nodes;
const devices = require("./device.controller.js");


// Retrieve INIT Node from the database.
exports.start = (req, res) => {
  // const name = "INIT";
  // var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  // // FIND INIT NODE
  // Node.find(condition)
  //   .then(data => {
  //     if (!data) {
  //       res.status(404).send({ message: "Not found INIT Node!"});

  //     }
  //     else {
  //       res.send(data);
  //     }
  //   })
  //   .catch(err => {
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while retrieving Nodes."
  //     });
  //   });

  Node.find({})
    .then(data => {
      if(data.length===0){
        this.createInit();
      }
      
    })
    .catch(err => {
      logger.info(err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Create Init Node due to setup the node of gate-node (after this node will be delete)
exports.createInit = (req,res,next) => {
  logger.info("INIT NODE");
  devices.createInit({'name':'INIT','type':'NODE_TEMP'},res,next);

  // REGISTER DEVICE /devices to MIDDLE-NODE
  // curl -X POST -d "type=init&ip=..." "http://192.168.193.116:1080/devices"
  // axios.post('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices', {
  //   type: 'NODO_TEMP'
  // })
  // .then((res) => {
  //   //logger.info(`statusCode: ${res.statusCode}`)
  //   logger.info("------> NODE INIT")
  //   const initNode = new Node({
  //     uuid: res.data.uuid,
  //     token: res.data.token,
  //     type: "NODO_TEMP"
  //   });
  //   // Save NODE_TEMP Node in the database
  //   initNode
  //     .save(initNode)
  //     .then(data => {
  //       updateDotenv({
  //         GATE_NODE_INIT: 'true'
  //       }, 'do_not_touch_file').then((newEnv) => logger.info('Done!', newEnv))
  //       res.send(data);
  //     })
  //     .catch(err => {
  //       //res.status(500).send({
  //       //  message:
  //       err || "Some error occurred while creating the Node."
  //       //});
  //     });
  // })
  // .catch((error) => {
  //   console.error(error)
  // })
};

// Retrieve INIT Node from the database.
exports.findInit = (req, res) => {
  const name = "INIT";
  var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  Node.find(condition)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found INIT Node!"});
      else res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Nodes."
      });
    });
};

// Create and Save a new Node for test
exports.testRegister = (req,res,next) => {
  devices.testRegister(req,res,next)
}
// Create and Save a new Node
exports.register = (req,res,next) => {
  
  var body = JSON.parse(req.body)
  
  // Validate request
  if (!body.name ) {
    res.status(400).send({ 
      message: `Some error occurred while creating the Node on local DB.`, 
      headertext: `Error!`
     });
    return;
  }
    
  devices.register(req,res,next)

  // device.register(req.name, search());

  
  // 2) INSIDE OF THE SEARCH CALL THE devices.register
  // 
  // SEARCH ARRAY OF DEVICE UUIDs BASEd ON KEY/VALUE QUERY CRITERIA
  // curl -X GET "http://192.168.193.116:1080/devices?type=init" --header "meshblu_auth_uuid: 6d99722f-0aab-4ae4-9372-299145c9fb06" --header "meshblu_auth_token: 34bb495a3e97fd20fbd729e232339a9869c0c649"  
  // axios.get('http://'+process.env.MIDDLENODE_HOSTNAME+':'+process.env.MIDDLENODE_HTTP_PORT+'/devices', {
  //   params: {
  //     name: req.body.name
  //   }
  // })
  // .then(function (response) {
  //   logger.info(response);
  // })
  // .catch(function (error) {
  //   logger.info(error);
  // })
  // .then(function () {
  //   // always executed
  // });  

  // CERCARE NODO CON LO STESSO NOME
  // SE NON SI TROVA REGISTRO NUOVO NODO
  //   POI CERCO NODO MANAGER ED ESEGUO UN UPDATE DEL NODO CON LE NUOVE INFORMAZIONI
  // DIVERSAMENTE INVIO NOTIFICA "ESISTE GIà QUESTO NOME"
  
  // Create a Node
  // const node = new Node({
  //   title: req.body.title,
  //   description: req.body.description,
  //   published: req.body.published ? req.body.published : false
  // });

  // // Save Node in the database
  // node
  //   .save(node)
  //   .then(data => {
  //     res.send(data);
  //   })
  //   .catch(err => {
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while creating the Node."
  //     });
  //   });
};

// Returns the Middle-Node platform status
exports.status = (req,res,next) => {
  logger.debug("STATUS")
  devices.status(req,res,next);
};

// Returns the Middle-Node platform status
exports.whoami = (req,res,next) => {
  logger.info("whoami");
  devices.whoami(req,res,next);

};

// Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
exports.messages = (req,res,next) => {
  devices.messages(req,res,next);

};

// This is a streaming API that returns device/node messages as they are sent and received. Meshblu doesn't close the stream.
exports.subscriptionsList = (req,res,next) => {
  logger.info("subscriptionsList");
  devices.subscriptionsList(req,res,next);
};

// This is a streaming API that returns device/node messages as they are sent and received. Meshblu doesn't close the stream.
exports.subscription = (req,res,next) => {
  logger.info("subscription");
  devices.subscription(req,res,next);
};

// This is a streaming API that returns device/node messages as they are sent and received. Meshblu doesn't close the stream.
exports.deleteSubscription = (req,res,next) => {
  devices.deleteSubscription(req,res);
};

// Retrieve all Node in array from middle-node network
exports.findAllArray = (req,res,next) => { 
  logger.info("GET findAllArray");
  //logger.info(req)
  devices.findAllArray(req,res,next);
};

// Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
exports.messages = (req,res,next) => {
  logger.info("MESSAGE")
  devices.messages(req,res,next);
};

// Retrieve all Node in array from middle-node network
exports.findByName = (req,res,next) => { 
  logger.info("GET findByName");
  //logger.info(req)
  devices.findByName(req,res,next);
};

// Retrieve all Nodes from middle-node network
exports.findAll = (req,res,next) => {
  logger.info("POST findAll")
  logger.info(req)
  devices.findAll(req,res);
};

// Find a single Node with an id
exports.findOne = (req,res,next) => {
  devices.findOne(req,res);
};
// Find a single Node with an id
exports.findOneV2 = (req,res,next) => {
  devices.findOneV2(req,res);
};


// Returns all information (including tokens) of all nodes or nodes belonging to a user's UUID (identified with an "owner" property and user's UUID i.e. "owner":"0d1234a0-1234-11e3-b09c-1234e847b2cc")
exports.findMyNodes = (req,res,next) => {
  devices.findMyDevices(req,res);
};

// Reset token of a single Node
exports.resetToken = (req,res,next) => {
  devices.resetToken(req,res);
};
// Update a Node by the id in the request
exports.update = (req,res,next) => {
  // if (!req.body) {
  //   return res.status(400).send({
  //     message: "Data to update can not be empty!"
  //   });
  // }

  devices.update(req,res);
};

// Update a Node by the id in the request
exports.updateV2_PUT = (req,res,next) => {
  logger.info("NODE updateV2_PUT")

  // logger.info(req)
  devices.updateV2_PUT(req,res,next);
};
// Update a Node by the id in the request
exports.updateV2_PATCH = (req,res,next) => {
  devices.updateV2_PATCH(req,res);
};

// Delete a Node with the specified id in the request
exports.delete = (req,res,next) => {
  devices.delete(req,res);
};

// Reset token of a single Node
exports.resetToken = (req,res,next) => {
  devices.resetToken(req,res);
};


// // Delete all Nodes from the database.
// exports.deleteAll = (req,res,next) => {
//   Node.deleteMany({})
//     .then(data => {
//       res.send({
//         message: `${data.deletedCount} Nodes were deleted successfully!`
//       });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while removing all Nodes."
//       });
//     });
// };

// // Find all published Nodes
// exports.findAllPublished = (req,res,next) => {
//   Node.find({ published: true })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving Nodes."
//       });
//     });
// };
