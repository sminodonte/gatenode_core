const logger = require('../plugins/logger');
const algorithms = require("../config/algorithms.config.js");

const db = require("../models");
const Algorithm = db.algorithms;


// Create and Save a new Algorithm
exports.create = (req, res) => {
  // Validate request
  // if (!req.body.title) {
  //   res.status(400).send({ message: "Content can not be empty!" });
  //   return;
  // }

  console.log("NEW ALGORITHM");
  console.log(req.body);

  // Create a Algorithm
  const algorithm = new Algorithm({
    title: req.body.title,
    //description: req.body.description,
  });

  // Save Algorithm in the database
  algorithm
    .save(algorithm)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Algorithm."
      });
    });

};

// Retrieve all Algorithms from the database.
exports.findAll = (req, res) => {

  res.json(algorithms)


  // const title = req.query.title;
  // var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  // Algorithm.find(condition)
  //   .then(data => {
  //     res.send(data);
  //   })
  //   .catch(err => {
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while retrieving Algorithms."
  //     });
  //   });
};
