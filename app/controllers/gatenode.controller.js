// SOCKETIO PUBLIC CONTROLLER FOR COMMUNICATION BETWEEN GATE-NODE AND MIDDLE-NODE

// SOCKETIO PRIVATE CONTROLLER FOR COMMUNICATION BETWEEN DAT AND GATE-NODE
const logger = require('../plugins/logger');
const devices = require("./device.controller.js");
const protocols = require("./protocols.controller.js");
const io = require("socket.io").listen(process.env.GATENODE_SOCKETIO_PORT);
let sequenceNumberByClientSocketio = new Map();

// CREATE SOCKETIO CONNECTION
exports.connect = (req, res) => {

  // SOCKETIO CONNECT
  io.on("connection", (socket) => {

  gatenode = socket;
  // console.log("gatenode after")
  // console.log(gatenode)
    logger.info(`Client gatenode connected [id=${gatenode.id}]`);
    // Initialize this client's sequence number
    sequenceNumberByClientSocketio.set(gatenode, 1);

    // Received data from DAT (Data Aquisition & Trasformation) to GATE-NODE     
    gatenode.on('gatenode', function (data) {
      logger.info("----------PROTOCOL gatenode----------");
      logger.info('Received data from DAT:');
      logger.info(JSON.stringify(data));
      //module.exports.zeromq();

      if(data.receiver.query==="name"){
        data.receiver.name.map(function (item) {
          logger.info("Receiver name node:");
          logger.info(item);
          var query = {"name": item}
          logger.info(query)
          
          logger.info("GET findByName");
          //logger.info(req)
          
          var req = {
            'query':{
              'uuid': process.env.GATE_NODE_DEVICE_UUID,
              'token': process.env.GATE_NODE_DEVICE_TOKEN,
              'name': item
            }
          } 
            
          devices.findByName(req,res, function(result){
            logger.info("Find device:");
            logger.info(result[0].uuid);

            logger.info("MESSAGE-----------");
             logger.info(process.env.GATE_NODE_DEVICE_UUID);
              logger.info(process.env.GATE_NODE_DEVICE_TOKEN);
               logger.info(data.sender);
                logger.info(data.topic);
                 logger.info(data.data.payload);

            //logger.info(JSON.stringify(JSON.parse(result[0])));

            var message={
              "uuid":process.env.GATE_NODE_DEVICE_UUID,
              "token":process.env.GATE_NODE_DEVICE_TOKEN, 
              "message":{
                "devices":[result[0].uuid],
                "sender": data.sender,
                "topic":data.topic,
                "time": Date.now(),
                "payload": data.data.payload
              }
            }
          
            logger.info("AFTER MESSAGE-----------");

            // Send data from GATE-NODE to MIDDLE-NODE 
            devices.sendMessages(message,res, function(result){
              logger.info("Response message:");
            });
          });
        });
      }
      if(data.receiver.query==="type"){
        var query = {"type": data.receiver.type}
        data.receiver.type.map(function (item) {
          logger.info("Receiver name node:");
          logger.info(item);
          var query = {"type": item}
          logger.info(query)
          
          logger.info("GET findByName");
          //logger.info(req)
          
          var req = {
            'query':{
              'uuid': process.env.GATE_NODE_DEVICE_UUID,
              'token': process.env.GATE_NODE_DEVICE_TOKEN,
              'type': item
            }
          } 
            
          devices.findByName(req,res, function(result){
            logger.info("Find device:");
            logger.info(result[0].uuid);
            //logger.info(JSON.stringify(JSON.parse(result[0])));

            var message={
              "uuid":process.env.GATE_NODE_DEVICE_UUID,
              "token":process.env.GATE_NODE_DEVICE_TOKEN, 
              "message":{
                "devices":[result[0].uuid],
                "sender": data.sender,
                "topic":data.topic,
                "time": Date.now(),
                "payload": data.data.payload
              }
            }

            logger.info("message");
            // Send data from GATE-NODE to MIDDLE-NODE 
            devices.sendMessages(message,res, function(result){
              logger.info("Response message:");
            });
          });
        });
       
      }
               
    });

    gatenode.on('gatenode_p2p', function (message) {

      logger.info("INSIDE GATENODE_P2P");

      // TODO:
      // 1) DALLA GUI SETTO IL NODO COME CLIENT O SERVER O IL TIPO DI PROTOCOLLO O PATTERN DI COMUNICAZIONE
      // 2) DA QUI FACCIO LA QUERY TO DB PER CAPIRE SE IL NODO è UN CLIENT O UN SERVER E IL PROTOCOLLO
      // 3) DAL RISULTATO DELLA QUERY CHIAMO IL METODO DEL CLIENT O SERVER DEL PROTOCOLLO CORRISPONDENTE

      if(message.data.role==="sender"){
        logger.info("SENDER");
        protocols.udp_client(message);
      }
      if(message.data.role==="receiver"){
        logger.info("RECEIVER");
        //protocols.udp_server();
      }

    });

    // When socket disconnects, remove it from the list:
    gatenode.on("disconnect", function () {
      sequenceNumberByClientSocketio.delete(gatenode);
      logger.info(`Client gatenode gone [id=${gatenode.id}]`);
      gatenode.removeAllListeners('gatenode');
      gatenode.removeAllListeners('disconnect');
      gatenode.removeAllListeners('connection');        
    });
      
  });


};
