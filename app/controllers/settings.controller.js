const logger = require('../plugins/logger');

// PER ORA NON C'è NESSUN COLLEGAMENTO AL DB SETTINGS MA SI LEGGE DA JSON FILE
// const db = require("../models");
// const Setting = db.settings;
const settings = require("../config/settings.config.js");
require('dotenv').config();
const db = require("../models");
const Node = db.nodes;


// Retrieve all Settings from the Json file.
// exports.findAllJson = (req, res) => {
//   res.json(settings)
// }

// Retrieve all Settings from the database.
exports.findAll = (req, res) => {

  Node.find()
    .then(data => {
      logger.info("READ SETTINGS:");
      logger.info(data)
      res.send(data);

    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Nodes."
      });
    });
};
