
const logger = require('../plugins/logger');

module.exports = {

  udp_server: function () {
    var PORT = 3333;
    var HOST = '0.0.0.0';

    var dgram = require('dgram');
    var server = dgram.createSocket('udp4');

    server.on('listening', function() {
      var address = server.address();
      logger.info('UDP Server listening on ' + address.address + ':' + address.port);
    });
    var counter = 0;
    server.on('message', function(message, remote) {
      counter++;  
      // EACH SSE ON GUI WITHOUT DIVIDE A TYPE OF DATA MESSAGE
      app.emit("bigdata", message)

      // EACH SSE ON GUI GONE DIVIDE BY TYPE OF DATA MESSAGE JUST FOR SAURON PROJECT
      if(JSON.parse(message).data.payload.algorithm==="crowd_counting"){
        logger.info("Algorithm crowd_counting:")
        logger.info('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
        app.emit("crowd_counting", message)
      }
      if(JSON.parse(message).data.payload.algorithm==="tracked_objects"){
        logger.info("Algorithm tracked_objects")
        logger.info('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
        app.emit("tracked_objects", message)
      }
      if(JSON.parse(message).data.payload.algorithm==="people_fluxs"){
        logger.info("Algorithm people_fluxs")
        logger.info('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
        app.emit("people_fluxs", message)
      }
      if(JSON.parse(message).data.payload.algorithm==="vehicle_fluxs"){
        logger.info("Algorithm vehicle_fluxs")
        logger.info('['+counter+'] '+remote.address + ':' + remote.port +' - ' + message);
        app.emit("vehicle_fluxs", message)
      }
    });

    server.bind(PORT, HOST);
  },
  udp_client: function (data) {
    var PORT = 3333;
    var HOST = '10.147.18.139';

    const dgram = require('dgram');
    
    const message = Buffer.from(JSON.stringify(data));
    const client = dgram.createSocket('udp4');

    //setInterval(function(){
    client.send(message, PORT, HOST, (err) => {
      logger.info('UDP message sent to ' + HOST +':'+ PORT + ' message:'+message);
      //  client.close();
    });
    //}, 500);

  },

  // CREATE PUBLISH SUBSCRIBE ZEROMQ CONNECTION
  zeromq_pubsub: function (data) {

    // pubber.js
    var zmq = require('zeromq')
      , sock = zmq.socket('pub');

    sock.bindSync('tcp://127.0.0.1:3333');
    logger.info('Publisher bound to port 3333');

    // setInterval(function(){
    //   logger.info('sending a multipart message envelope');
    //   sock.send(['gatenode', 'meow!']);
    // }, 500);

    // setInterval(function(){
    //   logger.info('sending a multipart message envelope');
    //   sock.send(['2_gatenode', 'bau!']);
    // }, 1000);
    
    // subber.js
    var zmq = require('zeromq')
      , sock = zmq.socket('sub');

    sock.connect('tcp://127.0.0.1:3000');
    sock.subscribe('gatenode');
    sock.subscribe('2_gatenode');
    logger.info('Subscriber connected to port 3000');

    sock.on('message', function(topic, message) {
      logger.info('received a message related to:', 
        `${topic}`, 'containing message:', `${message}`);
    });

  },

  // CREATE PEER-TO-PEER ZEROMQ CONNECTION
  zeromq_p2p_client: function(message) {
    //Peer Client code
    const zeromq = require(`zeromq`).socket(`pair`);
    const address = `tcp://10.147.18.139:3333`;
    logger.info(`Connecting to ${address}`);
    zeromq.connect(address);

    zeromq.on(`message`, function (msg) {
      logger.info(`Message received: ${msg}`);
    });
    logger.info("Send data to API_layer");
    zeromq.send(message);
  },

  // CREATE PEER-TO-PEER ZEROMQ CONNECTION
  zeromq_p2p_server: function() {
    //Peer Server Code
    const zeromq = require(`zeromq`).socket(`pair`);
    const address = 'tcp://*:3333';  
    logger.info(`Listening zeroMQ server at ${address}`);
    zeromq.bindSync(address);

    zeromq.on(`message`, function (msg) {
      logger.info(`Message received from nodes: ${msg}`);
    });

    //const sendMessage = function () {
        const message = `Sending ping from zeroMQ server`;
        logger.info('Sending ping from zeroMQ server');
        zeromq.send(message);
    //};
    //setInterval(sendMessage, 2000);

    // logger.info(`Sending 1 '${data}'`);
    // zeromq.send(data);
        
    // const sendMessage = function () {
    //     const message = `Ping 1`;
    //     logger.info(`Sending 1 '${message}'`);
    //     zeromq.send(message);
    // };
    //setInterval(sendMessage, 1000);

  },

  // CREATE PEER-TO-PEER WEBSOCKET CONNECTION
  websocket: function(data) {

  },

  // CREATE PEER-TO-PEER OSC CONNECTION
  osc: function(data) {

  },

}