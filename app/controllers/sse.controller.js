const logger = require('../plugins/logger');

// Subscribe event messages
exports.messages = (req, res, next) => {

  logger.info("SUBSCRIBE MESSAGES")
  let eventsSent = 1;

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("messages", data => {
  //app.on("message", data => {
    res.write(`id: ${eventsSent++}\n`);
    res.write(`event: messages\n`);
    res.write(`data: ` + `${data}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
    //event: dieRoll\ndata:${getRandomIndex(1, 6)}\n\n\n`);
  });

};

// Subscribe event bigdata
exports.bigdata = (req, res, next) => {

  logger.info("SUBSCRIBE BIG DATA")
  let eventsSent = 1;

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("bigdata", data => {
  //app.on("message", data => {
    res.write(`id: ${eventsSent++}\n`);
    res.write(`event: bigdata\n`);
    res.write(`data: ` + `${data}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
    //event: dieRoll\ndata:${getRandomIndex(1, 6)}\n\n\n`);
  });

};

// // Create and Save a new Tutorial
// exports.publish = (req, res, next) => {

//   console.log("PUBBLISH SSE")

//   res.writeHead(200, {
//     'Connection': 'keep-alive',
//     'Content-Type': 'text/event-stream',
//     'Cache-Control': 'no-cache'
//   });

//   const message = {
//     "message": "prova",
//     "timestamp": new Date()
//   }

//   // setInterval(function(){
//   //   console.log('writing');
//   //   res.write("id: " + Date.now() + "\ndata: " + message + "\n\n");
//   // }, 1000);

//   setInterval(() => {
//     req.app.emit("message", message)
//   }, 1000);
 
// };

// // exports.publish = (req, res, next) => {

//   console.log(req)

//   const message = {
//     "message": "prova",
//     "timestamp": new Date()
//    }

//   logger.info("message", message);
//   logger.info("json_human");
//   logger.info(json_human);

//   //req.app.emit("message", message)

//   // setInterval(() => {
//   // req.app.emit("message", message)

//   // }, 200);
//   setInterval(function(){
//       console.log('writing');
//       res.write("id: " + Date.now() + "\ndata: " + message + "\n\n");

//     }, 1000);
// };
