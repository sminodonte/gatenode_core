const logger = require('../plugins/logger');
const si = require('systeminformation');
var ipRangeCheck = require("ip-range-check");
require('dotenv').config({ path: 'env-file' });
const updateDotenv = require('../tools/update-dotenv');

let vpnIp;

// FIND IP DELLA MACCHINA CHE GLI ASSEGNA LA VPN  
exports.check = (req, res, next) => {

  si.networkInterfaces()
  .then(function(data) {
    for(i=0; i<data.length; i++){
      //console.log(data[i].ip4);
      if(ipRangeCheck(process.env.MIDDLENODE_HOSTNAME, data[i].ip4+"/24")){
        // console.log("IP VPN:");
       //  console.log("-------------")
        //console.log(data[i].ip4);
        vpnIp=data[i].ip4;

        // console.log("-------------")
      }
    }
    console.log("IP VPN:");
    console.log(vpnIp);
    updateDotenv({
      LOCAL_VPN_IP: vpnIp,
    }, '../../env-file').then((newEnv) => {
      console.log('LOCAL_VPN_IP saved in Env-file!', newEnv);
    })
  })
  .catch(error => console.error(error));
 
};
