const logger = require('../plugins/logger');
const json_human = require('../config/json_human.js');

// Subscribe event stream crowd counting
exports.crowd_counting = (req, res, next) => {

  logger.info("SUBSCRIBE CROWD COUNTING")

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("crowd_counting", data => {
  //app.on("message", data => {
    res.write(`event: crowd_counting\n`);
    res.write("id: " + "\ndata: " + `${JSON.stringify(data)}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
  });
};

// Subscribe event stream tracking
exports.tracked_objects = (req, res, next) => {

  logger.info("SUBSCRIBE TRACKED OBJECTS")

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("tracked_objects", data => {
  //app.on("message", data => {
    res.write(`event: tracked_objects\n`);
    res.write("id: " + "\ndata: " + `${JSON.stringify(data)}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
  });
};

// Subscribe event stream people fluxs
exports.people_fluxs = (req, res, next) => {

  logger.info("SUBSCRIBE PEOPLE FLUXS")

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("people_flux", data => {
  //app.on("message", data => {
    res.write(`event: people_flux\n`);
    res.write("id: " + "\ndata: " + `${JSON.stringify(data)}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
  });
};

// Subscribe event stream vehicle fluxs
exports.vehicle_fluxs = (req, res, next) => {

  logger.info("SUBSCRIBE VEHICLE FLUXS")

  res.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive"
  });
  
  req.app.on("vehicle_flux", data => {
  //app.on("message", data => {
    res.write(`event: vehicle_flux\n`);
    res.write("id: " + "\ndata: " + `${JSON.stringify(data)}\n\n`);
    //res.write(`data: ${JSON.stringify(data)}\n\n`);
  });
};
