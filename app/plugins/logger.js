'use strict';
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
const path = require('path');
require('dotenv').config({ path: 'env-file' });

const env = process.env.NODE_ENV || 'development';
const logDir = 'logs';

let level, silent;

switch (process.env.NODE_ENV) {
  case "production":
    level = "warning";
    silent = false;
    console.log('');
    console.log('');
    console.log('Gate-Node is in: ----->'+ process.env.NODE_ENV + '<----- mode!');
    console.log('');
    console.log('');
    console.log(' ---------------');
    console.log('| Logger types: |');
    console.log(' ---------------');
    console.log('production  - Logger file transport mode');
    console.log('console     - Logger console transport mode');
    console.log('development - Logger file and console transport mode');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('| -> Set NODE_ENV enviroment variable on env-file to modify the logger type |');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('');
    console.log('');
    break;
  case "console":
    level = "emerg";
    silent = false;    
    console.log('');
    console.log('');
    console.log('Gate-Node is in: ----->'+ process.env.NODE_ENV + '<----- mode!');
    console.log('');
    console.log('');
    console.log(' ---------------');
    console.log('| Logger types: |');
    console.log(' ---------------');
    console.log('production  - Logger file transport mode');
    console.log('console     - Logger console transport mode');
    console.log('development - Logger file and console transport mode');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('| -> Set NODE_ENV enviroment variable on env-file to modify the logger type |');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('');
    console.log('');
    break;
  default:
    level = "development";
    silent = false;  
    console.log('');
    console.log('');
    console.log('Gate-Node is in: ----->'+ process.env.NODE_ENV + '<----- mode!');
    console.log('');
    console.log('');
    console.log(' ---------------');
    console.log('| Logger types: |');
    console.log(' ---------------');
    console.log('production  - Logger file transport mode');
    console.log('console     - Logger console transport mode');
    console.log('development - Logger file and console transport mode');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('| -> Set NODE_ENV enviroment variable on env-file to modify the logger type |');
    console.log(' ---------------------------------------------------------------------------');  
    console.log('');
    console.log('');
    break;
}

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const custom = format.printf((info) => {
  return `${info.timestamp} [ ${info.level} ] [${info.label}]: ${info.message}`;
});

const dailyRotateFileTransport = new transports.DailyRotateFile({
  level: process.env.ENV === 'development' ? 'debug' : 'info',
  filename: `${logDir}/log_file_%DATE%.log`,
  datePattern: 'DD-MM-YYYY-ddTHH',
  zippedArchive: true,
  timestamp: true,
  handleExceptions: true,
  humanReadableUnhandledException: true,
  prettyPrint: true,
  json: true,
  maxsize: 5242880, // 5MB
  colorize: true,
  options: { flags: 'w' } // rewrite file and not append
});

const consoleTransport = new transports.Console({
  level: 'debug',
  handleExceptions: true,
  json: true,
  colorize: true,
  format: format.combine(
    format.colorize(),
    custom
  )
});

const logger = createLogger({
  // change level if in dev environment versus production
  level: level,
  format: format.combine(
    format.label({ label: path.basename(process.mainModule.filename) }),
    format(info => {
      info.level = info.level.toUpperCase()
      return info;
    })(),
    format.timestamp({
      format: 'DD-MM-YYYY HH:mm:ss'
    }),
    custom
  ),
  transports: [
    consoleTransport,
    dailyRotateFileTransport
  ],
  silent: silent // If true, all logs are suppressed
});

if(process.env.NODE_ENV==='production'){
  logger.remove(consoleTransport); // Remove console transport
}
if(process.env.NODE_ENV==='console'){
  logger.remove(dailyRotateFileTransport); // Remove console transport
}

module.exports = logger;

// to set enviroment variable export NODE_ENV= valore
// list enviroment variable env