module.exports = app => {
  const nodes = require("../controllers/node.controller.js");

  var router = require("express").Router();

  router.get("/start", nodes.start);
  // router.post("/init", nodes.createInit);
  router.get("/init", nodes.findInit);
  /**
   * @swagger
   * /nodes:
   *    post:
   *      tags:
   *          - Nodes
   *      summary: Register a node for test
   *      description: Register a node to middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Register a node to middle-node network.
   */
  // router.post("/", nodes.testRegister);
  /**
   * @swagger
   * /nodes:
   *    post:
   *      tags:
   *          - Nodes
   *      summary: Register a node.
   *      description: Register a node to middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Register a node to middle-node network.
   */
  router.post("/", nodes.register);

  /**
   * @swagger
   * /nodes:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Retrieve all nodes in a array of device UUIDs.
   *      description: Retrieve all nodes in a array of device UUIDs of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              key:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns in a array of nodes UUIDs based on key/value query criteria
   */
  router.get("/findAllArray", nodes.findAllArray);

  /**
   * @swagger
   * /nodes:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Retrieve all nodes in a array of device UUIDs.
   *      description: Retrieve all nodes in a array of device UUIDs of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              key:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns in a array of nodes UUIDs based on key/value query criteria
   */
  router.get("/findByName", nodes.findByName);

  /*
   * @swagger
   * /nodes:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Retrieve all nodes.
   *      description: Retrieve all node of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve all nodes.
   */
  router.post("/findAll", nodes.findAll);

  /**
   * @swagger
   * /nodes/{uuid}:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Retrieve a single Node with id.
   *      description: Retrieve a single Node with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve a single Node with id.
   */
  router.get("/:uuid", nodes.findOne);
  router.get("/:uuid", nodes.findOneV2);


   /**
   * @swagger
   * /nodes/{uuid}:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Returns all information of all  my Node.
   *      description: Returns all information (including tokens) of all devices or nodes belonging to a user's UUID (identified with an "owner" property and user's UUID i.e. "owner":"0d1234a0-1234-11e3-b09c-1234e847b2cc")
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve a single Node with id.
   */
  router.get("/", nodes.findMyNodes);

  router.get("/status", nodes.status);


  /**
   * @swagger
   * /nodes/whoami:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Returns information about the currently authenticated device.
   *      description: Returns information about the currently authenticated device.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/whoami", nodes.whoami);

  /**
   * @swagger
   * /nodes/messages:
   *    post:
   *      tags:
   *          - Nodes
   *      summary: Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
   *      description: Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/messages", nodes.messages);

  /**
   * @swagger
   * /nodes/subscriptions:
   *    get:
   *      tags:
   *          - Subscriptions
   *      summary: Get a list of subscriptions.
   *      description: Get a list of server-side subscriptions from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/subscriptions/list", nodes.subscriptionsList);

  /**
   * @swagger
   * /nodes/subscriptions:
   *    delete:
   *      tags:
   *          - Subscriptions
   *      summary: Remove subscription.
   *      description: Remove a server-side subscription from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/subscriptions/delete", nodes.deleteSubscription);

  /**
   * @swagger
   * /nodes/subscriptions:
   *    post:
   *      tags:
   *          - Subscriptions
   *      summary: Create subscription.
   *      description: Create a server-side subscription from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/subscriptions", nodes.subscription);

  // Retrieve all published nodes
  // router.get("/published", nodes.findAllPublished);

  /**
   * @swagger
   * /nodes/{id}:
   *    put:
   *      tags:
   *          - Nodes
   *      summary: Update a Node with id.
   *      description: Update a Node with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Update a Node with id.
   */
  router.put("/:id", nodes.update);
  router.post("/update", nodes.updateV2_PUT);
  router.put("/:id", nodes.updateV2_PATCH);
  /**
   * @swagger
   * /nodes/{id}:
   *    delete:
   *      tags:
   *          - Nodes
   *      summary: Delete a Node with id.
   *      description: Delete a Node with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Delete a Node with id.
   */
  router.delete("/", nodes.delete);

  /**
   * @swagger
   * /nodes:
   *    post:
   *      tags:
   *          - Nodes
   *      summary: Reset token of a Node.
   *      description: Reset token of a Node.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Delete a Node with id.
   */
  router.post("/", nodes.resetToken);

  // Create a new Node
  // router.delete("/", nodes.deleteAll);

  app.use("/api/nodes", router);
};
