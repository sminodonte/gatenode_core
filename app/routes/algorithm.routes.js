module.exports = app => {
  const algorithms = require("../controllers/algorithm.controller.js");

  var router = require("express").Router();

  /**
   * @swagger
   *   components:
   *     schemas:
   *       Algorithm:
   *         type: object
   *         properties:
   *            title:
   *              type: array
   *              items:
   *                type: string
   */
  
  /**
   * @swagger
   * /algorithms:
   *    get:
   *      tags:
   *          - Algorithms
   *      summary: Lists all the algorithms available.
   *      responses:
   *        200:
   *          description: The list of algorithms.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Algorithm'
   */

  router.get("/", algorithms.findAll);

  //   *
  //  * @swagger
  //  * /algorithms:
  //  *    post:
  //  *      tags:
  //  *          - Algorithms
  //  *      summary: Creates a new algorithm.
  //  *      description: 
  //  *      consumes:
  //  *        - application/json
  //  *      requestBody:
  //  *        required: true
  //  *        content:
  //  *          application/json:
  //  *            schema:
  //  *              $ref: '#/components/schemas/Algorithm'
  //  *      responses:
  //  *        200:
  //  *          description: The created algorithm.
  //  *          content:
  //  *            application/json:
  //  *              schema:
  //  *                $ref: '#/components/schemas/Algorithm'
   

  // router.post("/", algorithms.create);

  app.use("/api/v1/algorithms", router);
};
