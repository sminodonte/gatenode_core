module.exports = app => {
  const socketio = require("../controllers/socketio.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.post("/", socketio.create);

  // // Retrieve all socketio
  // router.get("/", socketio.findAll);

  // // Retrieve all published socketio
  // router.get("/published", socketio.findAllPublished);

  // // Retrieve a single Tutorial with id
  // router.get("/:id", socketio.findOne);

  // // Update a Tutorial with id
  // router.put("/:id", socketio.update);

  // // Delete a Tutorial with id
  // router.delete("/:id", socketio.delete);

  // // Create a new Tutorial
  // router.delete("/", socketio.deleteAll);

  app.use("/api/socketio", router);
};
