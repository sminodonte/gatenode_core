module.exports = app => {
  const systems = require("../controllers/system.controller.js");
  var router = require("express").Router();

  /**
   * @swagger
   *   components:
   *     schemas:
   *       System:
   *         type: object
   *         required:
   *           - algorithms
   *           - sources
   *         properties:
   *            algorithms:
   *              type: array
   *              items:
   *                type: string
   *            sources:
   *              type: array
   *              items:
   *                type: string
   *         example:   
   *            algorithms: ["crowd_counting","people_flux"]
   *            sources: ["http://video.com"]
   */
  
  /**
   * @swagger
   *   components:
   *     schemas:
   *       Stream:
   *         type: object
   *         required:
   *           - title
   *         properties:
   *            title:
   *              type: string 
   */
  
  /**
   * @swagger
   * /systems:
   *    post:
   *      tags:
   *          - Systems
   *      summary: Start data acquisition from algorithms for one source streams.
   *      description: 
   *      consumes:
   *        - application/json
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/System'
   *      responses:
   *        200:
   *          description: Started data acquisition.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/System'
   */
  router.post("/", systems.start);

  /**
   * @swagger
   * /systems:
   *    get:
   *      tags:
   *          - Systems
   *      summary: Stop data acquisition of all algorithms for all video streams.
   *      responses:
   *        200:
   *          description: Stopped data acquisition.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/System'
   */
  router.get("/", systems.stop);

  app.use("/api/v1/systems", router);
};
