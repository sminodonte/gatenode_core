module.exports = app => {
  const sources = require("../controllers/source.controller.js");

  var router = require("express").Router();

  /**
   * @swagger
   *   components:
   *     schemas:
   *       Source:
   *         type: object
   *         properties:
   *            url:
   *              type: array
   *              items:
   *                type: string
   */
  
  /**
   * @swagger
   * /sources:
   *    get:
   *      tags:
   *          - Sources
   *      summary: Lists all the sources available.
   *      responses:
   *        200:
   *          description: The list of sources.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Source'
   */
  
  router.get("/", sources.findAll);

  app.use("/api/v1/sources", router);
};
