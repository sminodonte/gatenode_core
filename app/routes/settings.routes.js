module.exports = app => {
  const settings = require("../controllers/settings.controller.js");

  var router = require("express").Router();

  // Retrieve all Settings
  router.get("/", settings.findAll);

  app.use("/api/settings", router);

};
