module.exports = app => {
  const devices = require("../controllers/device.controller.js");

  var router = require("express").Router();

  //router.get("/start", devices.start);

  /**
   * @swagger
   * /devices:
   *    post:
   *      tags:
   *          - Devices
   *      summary: Register a node for test.
   *      description: Register a node to middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              name:
   *                type: string
   *      responses:
   *        200:
   *          description: Register a node to middle-node network.
   */
  // router.post("/", devices.testRegister);
  /**
   * @swagger
   * /devices:
   *    post:
   *      tags:
   *          - Devices
   *      summary: Register a node.
   *      description: Register a node to middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              name:
   *                type: string
   *      responses:
   *        200:
   *          description: Register a node to middle-node network.
   */
  router.post("/", devices.register);
  /**
   * @swagger
   * /devices:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Retrieve all devices in a array of device UUIDs.
   *      description: Retrieve all device in a array of device UUIDs of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              key:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns in a array of device UUIDs based on key/value query criteria
   */
  router.get("/", devices.findAllArray);

    /**
   * @swagger
   * /nodes:
   *    get:
   *      tags:
   *          - Nodes
   *      summary: Retrieve all nodes in a array of device UUIDs.
   *      description: Retrieve all nodes in a array of device UUIDs of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              key:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns in a array of nodes UUIDs based on key/value query criteria
   */
  router.get("/findByName", devices.findByName);
  /*
   * @swagger
   * /devices:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Retrieve all devices.
   *      description: Retrieve all devices of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve all devices.
   */
  router.post("/", devices.findAll);

  /**
   * @swagger
   * /devices/{uuid}:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Retrieve a single Device with id.
   *      description: Retrieve a single Device with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve a single Device with id.
   */
  router.get("/:uuid", devices.findOne);
  router.get("/:uuid", devices.findOneV2);
      /**
   * @swagger
   * /devices/{uuid}:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Returns all information of all  my devices.
   *      description: Returns all information (including tokens) of all devices or devices belonging to a user's UUID (identified with an "owner" property and user's UUID i.e. "owner":"0d1234a0-1234-11e3-b09c-1234e847b2cc")
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Retrieve a single Node with id.
   */
  router.get("/mydevices", devices.findMyDevices);

  /**
   * @swagger
   * /nodes/whoami:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Returns information about the currently authenticated device.
   *      description: Returns information about the currently authenticated device.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.get("/whoami", devices.whoami);

  /**
   * @swagger
   * /devices/status:
   *    get:
   *      tags:
   *          - Devices
   *      summary: Returns the Middle-Node platform status.
   *      description: Returns the Middle-Node platform status.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.get("/status", devices.status);

  /**
   * @swagger
   * /devices/messages:
   *    post:
   *      tags:
   *          - Devices
   *      summary: Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
   *      description: Send a message to a specific device, array of devices, or all devices subscribing to a UUID on the Middle-Node platform.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.get("/messages", devices.messages);

  /**
   * @swagger
   * /devices/subscribe:
   *    get:
   *      tags:
   *          - Subscriptions
   *      summary: Get a list of subscriptions.
   *      description: Get a list of server-side subscriptions from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.get("/subscriptions", devices.subscriptionsList);

  /**
   * @swagger
   * /devices/subscribe:
   *    post:
   *      tags:
   *          - Subscriptions
   *      summary: Create subscription.
   *      description: Create a server-side subscription from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.post("/subscriptions", devices.subscription);

  /**
   * @swagger
   * /devices/subscribe:
   *    post:
   *      tags:
   *          - Subscriptions
   *      summary: Remove subscription.
   *      description: Remove a server-side subscription from one device to another.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Returns the Middle-Node platform status.
   */
  router.delete("/subscriptions", devices.deleteSubscription);

  // Retrieve all published device
  // router.get("/published", devices.findAllPublished);

  /**
   * @swagger
   * /devices/{id}:
   *    put:
   *      tags:
   *          - Devices
   *      summary: Update a Node with id.
   *      description: Update a Node with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Update a Node with id.
   */
  router.put("/:uuid", devices.update);
  router.put("/update", devices.updateV2_PUT);
  router.put("/:uuid", devices.updateV2_PATCH);
  /**
   * @swagger
   * /devices/{id}:
   *    delete:
   *      tags:
   *          - Devices
   *      summary: Delete a Node with id.
   *      description: Delete a Node with id of middle-node network.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Delete a Node with id.
   */
  router.delete("/:uuid", devices.delete);

    /**
   * @swagger
   * /devices:
   *    post:
   *      tags:
   *          - Devices
   *      summary: Reset token of a Node.
   *      description: Reset token of a Node.
   *      consumes:
   *        - application/json
   *      parameters:
   *        - name: body
   *          in: body
   *          schema:
   *            type: object
   *            properties:
   *              flavor:
   *                type: string
   *      responses:
   *        200:
   *          description: Delete a Node with id.
   */
  router.post("/", devices.resetToken);

  // Create a new Node
  // router.delete("/", devices.deleteAll);

  app.use("/api/devices", router);
};
