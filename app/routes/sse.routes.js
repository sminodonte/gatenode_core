module.exports = app => {
  const sse = require("../controllers/sse.controller.js");

  var router = require("express").Router();

  // Subscribe event stream
  // curl -XGET "http://localhost:4300/api/sse/eventstream"
  router.get("/messages", sse.messages);

  //router.get("/stream", sse.subscribe);


  // Publish a "message" event
  // curl -XPOST "http://localhost:4300/api/sse/message" -d "some message"
  router.get("/bigdata", sse.bigdata);

  // // Retrieve all Algorithms
  // router.get("/", algorithms.findAll);

  // // Retrieve all Cams
  // router.get("/", cams.findAll);

  // // Retrieve all Videos
  // router.get("/", videos.findAll);

  app.use("/api/sse", router);
};
