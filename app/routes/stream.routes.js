module.exports = app => {
  const streams = require("../controllers/stream.controller.js");

  var router = require("express").Router();

    /**
   * @swagger
   *   components:
   *     schemas:
   *       Stream_crowd_counting:
   *         type: object
   *         properties:
   *            algorithm:
   *              type: string
   *            payload:
   *              type: object 
   *              properties:
   *                created_at:
   *                  type: string
   *                crowd_count: 
   *                  type: number
   *                crowd_estimation: 
   *                  type: number
   *                crowd_image: 
   *                  type: string 
   *                data: 
   *                  type: string 
   *                density_image: 
   *                  type: string
   *                heatmap_image:
   *                  type: string 
   *                id: 
   *                  type: number 
   *                morphing_image: 
   *                  type: string
   *                notes: 
   *                  type: string
   *                resolution: 
   *                  type: string
   *                updated_at: 
   *                  type: string            
   *         example:   
   *            algorithm: crowd_counting
   *            payload: {
                  "created_at": "2020-10-09T17:06:44.018956", 
                  "crowd_count": null, 
                  "crowd_estimation": 769, 
                  "crowd_image": "c51aa2b9-26b4-4f43-8b4f-0a7ba67e0525/checkered6.jpg", 
                  "data": null, 
                  "density_image": "c51aa2b9-26b4-4f43-8b4f-0a7ba67e0525/density_map.jpg", 
                  "heatmap_image": "c51aa2b9-26b4-4f43-8b4f-0a7ba67e0525/heat_map.jpg", 
                  "id": 502, 
                  "morphing_image": "c51aa2b9-26b4-4f43-8b4f-0a7ba67e0525/morphing.jpg", 
                  "notes": null, 
                  "resolution": "1600x1065", 
                  "updated_at": "2020-10-09T17:06:49.039855"
  }
   */
  
  /**
   * @swagger
   *   components:
   *     schemas:
   *       Stream:
   *         type: object
   *         properties:
   *            algorithm:
   *              type: string
   *            payload:
   *              type: object 
   */



  /**
   * @swagger
   * /streams/crowd_counting:
   *    get:
   *      tags:
   *          - Streams
   *      summary: Data flux of counting and estimating the density of people as SSE stream.
   *      responses:
   *        200:
   *          description: The source is streaming.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Stream_crowd_counting'            
   */
  router.get("/crowd_counting", streams.crowd_counting);

  /**
   * @swagger
   * /streams/tracking:
   *    get:
   *      tags:
   *          - Streams
   *      summary: Data flux of detection and tracking of people and vehicles as SSE stream.
     *      responses:
   *        200:
   *          description: The source is streaming.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Stream'
   */
  router.get("/tracked_objects", streams.tracked_objects);

  /**
   * @swagger
   * /streams/people_flux:
   *    get:
   *      tags:
   *          - Streams
   *      summary: People flux analysis as SSE stream.
   *      responses:
   *        200:
   *          description: The source is streaming.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Stream'
   */
  router.get("/people_fluxs", streams.people_fluxs);

  /**
   * @swagger
   * /streams/vehicle_flux:
   *    get:
   *      tags:
   *          - Streams
   *      summary: Vehicle flux analysis as SSE stream.
   *      responses:
   *        200:
   *          description: The source is streaming.
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/Stream'
   */
  router.get("/vehicle_fluxs", streams.vehicle_fluxs);

  app.use("/api/v1/streams", router);
};
