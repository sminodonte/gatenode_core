// Read env-file 
require('dotenv').config({ path: 'env-file' })

module.exports = {
  setting:"settings from server", 
  init: process.env.GATE_NODE_INIT,
  uuid: process.env.GATE_NODE_DEVICE_UUID,
  token: process.env.GATE_NODE_DEVICE_TOKEN,
};