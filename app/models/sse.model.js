module.exports = mongoose => {
  var schema = mongoose.Schema(
    {
    message: {
      type: {
    type: String
    },
    },
    timestamp: {
      type: Date,
      default: new Date(),
      expires: 30 // seconds
    }
  },
  { versionKey: false }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Sse = mongoose.model("sse", schema);
  return Sse;
};
