const dbConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
//db.tutorials = require("./tutorial.model.js")(mongoose);
//db.sse = require("./sse.model.js")(mongoose);
db.nodes = require("./node.model.js")(mongoose);
db.devices = require("./device.model.js")(mongoose);
db.algorithm = require("./algorithm.model.js")(mongoose);
db.source = require("./source.model.js")(mongoose);


module.exports = db;
