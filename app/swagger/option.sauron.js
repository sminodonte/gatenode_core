// SWAGGER OPTIONS FOR SAURON PRO
module.exports = {
  swaggerDefinition: {
    //swagger: "2.0",
    openapi: "3.0.0",
    components: {},
    info: {
      title: "Sauron APIs layer",
      version: "1.0",
      description:
        "Endpoints to test the Sauron routes.",
      // license: {
      //   name: "MIT",
      //   url: "https://spdx.org/licenses/MIT.html",
      // },
      // contact: {
      //   name: "Paolo Moi",
      //   email: "pmoi@crs4.it",
      // },
    },
    host: 'localhost:4100',
    basePath: '/api/v1',
    servers: [
      {
        url: "http://localhost:4100/api/v1"
      }
    ],
  },
  apis: [
    "./app/routes/system.routes.js",
    "./app/routes/algorithm.routes.js",
    "./app/routes/source.routes.js",
    "./app/routes/stream.routes.js"
  ],
};