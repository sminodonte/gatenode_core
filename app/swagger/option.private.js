// SWAGGER OPTIONS FOR GATE-NODE
module.exports = {
  definition: {
    swagger: "2.0",
    // openapi: "3.0.0",
    components: {},
    info: {
      title: "Middle-Node APIs",
      version: "1.0",
      description:
        "Endpoints to test the Middle-Node routes.",
      // license: {
      //   name: "MIT",
      //   url: "https://spdx.org/licenses/MIT.html",
      // },
      contact: {
        name: "Paolo Moi",
        email: "pmoi@crs4.it",
      },
    },
    host: 'localhost:4100',
    basePath: '/',
    servers: [
      {
        url: "http://localhost:4100",
      },
    ],
  },
  apis: ["./app//routes/device.routes.js"],
};