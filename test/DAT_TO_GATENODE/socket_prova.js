var socket = require('socket.io-client')('http://localhost:3200');
var data = {
  "sender": "node0",
  "topic": "TEST",
  "receiver": {
    "name": ["nodeA","nodeB","nodeB","nodeD"],
    "type": "compute"
  },
  "data": {
    "message": {
      "payload": {"online":"message"}
    },
    "stream": {
      "payload": {"online":"stream"}
    }
  },
  "query": "name"
}
socket.on("connection", socket => {
  // Log whenever a user connects
  console.log("user connected");
    socket.emit("gatenode", data);

  // Log whenever a client disconnects from our websocket server
  socket.on("disconnect", function() {
    console.log("user disconnected");
  });

  // When we receive a 'message' event from our client, print out
  // the contents of that message and then echo it back to our client
  // using `io.emit()`
  socket.on("gatenode", message => {
    console.log("Message Received: " + message);
    socket.emit("gatenode", { type: "new-message", text: message });
  });
});

