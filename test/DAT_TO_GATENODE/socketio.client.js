// Example of communication between DAT (Data Aquisition & Trasformation) and GATE-NODE 

var socketio = require('socket.io-client')('http://127.0.0.1:3200');
var d = new Date();
var sending_time = d.getTime();

// Example of data to send
var data = {
  "sender": "node0",
  "topic": "TEST",
  "receiver": {
    "name": ["nodeA","nodeB"],
    "type": "compute"
  },
  "data": {
    "message": {
      "payload": {"online":"message"}
    },
    "stream": {
      "payload": {"online":"stream"}
    }
  },
  "query": "name"
}

console.log(data);
socketio.on('connect', () => {
  console.log('Connect to GATE-NODE successful')
})

socketio.on('disconnect', () => {
  console.log('disconnect to GATE-NODE')
})
// Sent data from DAT (Data Aquisition & Trasformation) to GATE-NODE 
socketio.emit('gatenode', data, e => {
  console.log('Sent data to GATE-NODE: ', e, data);
})

// Received data from GATE-NODE to DAT    
socketio.on('gatenode', (data) => {
  console.log('Received data from GATE-NODE: ',data);
});

socketio.on('error', (e) => {
  console.log('Error: '+e)
})

socketio.on('connect_failed', () => {
   document.write("Sorry, there seems to be an issue with the connection!");
})