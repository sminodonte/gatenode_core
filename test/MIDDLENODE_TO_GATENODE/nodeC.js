//  DEVICE A localhost
const DEVICE_0_UUID = "803daf7b-c897-49be-9790-d707a9af69a7";
const DEVICE_0_TOKEN = "0a6016187e14b62170c94c1a007345f5fb2586ed";
const DEVICE_A_UUID = "546422c7-aa7e-4a6a-a001-9093b1d137b1";
const DEVICE_A_TOKEN = "e207e1d9135cdcc1b43504ef4d82c11b5ba9a4ac";
const DEVICE_B_UUID = "422dd26a-0638-4483-b60a-90b85459e386"; // UUID ERRATO
const DEVICE_B_TOKEN = "964a0a42baa672cd25a06b14e3b391c866798622"
const DEVICE_C_UUID = "974a3571-047c-46ff-ac9b-a57275ef4b66";
const DEVICE_C_TOKEN = "782038583840b726860bd509b2ff229b781f244f"
const DEVICE_D_UUID = "72218be0-ae6d-446e-978f-11045c7d842a";
const DEVICE_D_TOKEN = "a35d74a3c4e95f19c601d0a3d14828ee05ec0b06"

var config = {
  "server": "localhost",
  "port": 4080,
  "uuid": DEVICE_A_UUID,
  "token": DEVICE_A_TOKEN
};


var debug = require("debug")('socket.io-client');
var socketIoClient = require('socket.io-client');

var socket = socketIoClient("ws://192.168.193.116:4080", {path: "/socket.io/v2"});

socket.on("connect", function(){
  socket.emit('identity', {
          uuid: DEVICE_C_UUID,
          token: DEVICE_C_TOKEN
  }, function(res){
    console.log(res);
  });

  socket.on('ready', function(){

    socket.emit("whoami", {}, function(res){
            console.log(res);
    });

    socket.emit("message", {
      "devices": '*',
      "topic":"SENT FROM DEVICE_A_UUID",
      "time": Date.now(),
      "payload":{
        "online":true
      }
    });
  });

  socket.on('message', function(data){
    console.log(data);
  });
  
});
