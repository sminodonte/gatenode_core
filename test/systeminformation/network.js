const si = require('systeminformation');

// promises style - new since version 3
// si.cpu()
//   .then(data => console.log(data))
//   .catch(error => console.error(error));

si.networkInterfaces()
	.then(data => console.log(data))
  .catch(error => console.error(error));

console.log("------");
console.log("------");

// si.networkInterfaceDefault().then(data => console.log(data));

// si.networkGatewayDefault().then(data => console.log(data));

// si.networkConnections().then(data => console.log(data));


// si.wifiNetworks().then(data => console.log(data));

