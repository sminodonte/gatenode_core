//Peer Server Code
const zeromq = require(`zeromq`).socket(`pair`);
const address = `tcp://127.0.0.1:3000`;  
console.log(`Listening at ${address}`);
zeromq.bindSync(address);

zeromq.on(`message`, function (msg) {
    console.log(`Message received 2: ${msg}`);
});

const sendMessage = function () {
    const message = `Ping 1`;
    console.log(`Sending 1 '${message}'`);
    zeromq.send(message);
};
setInterval(sendMessage, 1000);

