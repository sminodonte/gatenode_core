// pubber.js
var zmq = require('zeromq')
  , sock = zmq.socket('pub');

sock.bindSync('tcp://127.0.0.1:3000');
console.log('Publisher bound to port 3000');

setInterval(function(){
  console.log('sending a multipart message envelope');
  sock.send(['gatenode', 'meow!']);
}, 500);

setInterval(function(){
  console.log('sending a multipart message envelope');
  sock.send(['2_gatenode', 'bau!']);
}, 1000);