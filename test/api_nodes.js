const nodes = require("../app/controllers/node.controller.js");

// MIDDLE-NODE IN LOCALHOST:
const DEVICE_0_UUID = "b447f3ed-3fe7-439e-ac6b-c7c8b300478";
const DEVICE_0_TOKEN = "6762f27cf67bc50c86c02a0efb59a6028fa8b633";
const DEVICE_A_UUID = "546422c7-aa7e-4a6a-a001-9093b1d137b1";
const DEVICE_A_TOKEN = "e207e1d9135cdcc1b43504ef4d82c11b5ba9a4ac";
const DEVICE_B_UUID = "422dd26a-0638-4483-b60a-90b85459e386"; 
const DEVICE_B_TOKEN = "964a0a42baa672cd25a06b14e3b391c866798622"
const DEVICE_C_UUID = "974a3571-047c-46ff-ac9b-a57275ef4b66";
const DEVICE_C_TOKEN = "782038583840b726860bd509b2ff229b781f244f"
const DEVICE_D_UUID = "72218be0-ae6d-446e-978f-11045c7d842a";
const DEVICE_D_TOKEN = "a35d74a3c4e95f19c601d0a3d14828ee05ec0b06"

// API TEST:
  // NODES:
  // nodes.testRegister({"name":"nodeA", "type":"view", "meshblu" : {"version" : "2.0.0","whitelists" : {"message" : {"from" : []},"discover" : {"as" : [ {"uuid" : "*"}],"view" : [{"uuid" : "*"}]}}}});
  // nodes.testRegister({"name":"nodeB", "type":"view", "meshblu" : {"version" : "2.0.0","whitelists" : {"message" : {"from" : []},"discover" : {"as" : [ {"uuid" : "*"}],"view" : [{"uuid" : "*"}]}}}});
  // nodes.testRegister({"name":"nodeC", "type":"view", "meshblu" : {"version" : "2.0.0","whitelists" : {"message" : {"from" : []},"discover" : {"as" : [ {"uuid" : "*"}],"view" : [{"uuid" : "*"}]}}}});
  // nodes.testRegister({"name":"nodeD", "type":"view", "meshblu" : {"version" : "2.0.0","whitelists" : {"message" : {"from" : []},"discover" : {"as" : [ {"uuid" : "*"}],"view" : [{"uuid" : "*"}]}}}});
  //nodes.start();

  //nodes.whoami({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  // nodes.status();
  //nodes.findOne({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  //nodes.findOneV2({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});

  //nodes.findMyNodes({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  //nodes.findAll({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"user":"robot"});
  //nodes.findAllArray({"headers":{"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN}});
  //nodes.update({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"type":"NODE_TEMP"});
  //nodes.delete({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  //nodes.resetToken({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  
  // TEST MESSAGES: 
    // Send Direct Message:
      // 1)
      // nodes.subscription({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"emitterUuid": DEVICE_A_UUID, "subscriberUuid": DEVICE_A_UUID, "type":"message.received"});
      // nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_UUID,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": DEVICE_B_UUID, "type":"message.received"});
      // nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_UUID,"emitterUuid": DEVICE_C_UUID, "subscriberUuid": DEVICE_C_UUID, "type":"message.received"});
      // nodes.subscription({"uuid":DEVICE_D_UUID,"token":DEVICE_D_UUID,"emitterUuid": DEVICE_D_UUID, "subscriberUuid": DEVICE_D_UUID, "type":"message.received"});
      // 2)
      // AGGIUNGERE NELL'ARRAY GLI UUID DEI NODI DA ABILITARE "meshblu" : {"version" : "2.0.0","whitelists" : {"message" : {"from" : [{"uuid":""}] }.....
      // 
      // 
      // Scenario: Device B allows A to send a message: 
      // UPDATE ALWAYS ALL ELEMENT THAT YOU INSERT IN $SET 
      //nodes.updateV2_PUT({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"data":{"$set":{"meshblu.whitelists.message.from":[{"uuid": DEVICE_C_UUID},{"uuid": DEVICE_B_UUID}]}}});  

      // PUSH ELEMENT ON ARRAY
      //nodes.updateV2_PUT({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"data":{"$push":{"meshblu.whitelists.message.from":{"uuid": DEVICE_C_UUID}  }}});  
      // PULL ELEMENT ON ARRAY
      //nodes.updateV2_PUT({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"data":{"$push":{"meshblu.whitelists.message.from":{"uuid": DEVICE_C_UUID}  }}});  


       // nodes.updateV2_PUT({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"data": {""$push: {"from": {"uuid": DEVICE_A_UUID}}}});  
      // OPTIONAL:
     //nodes.subscription({"uuid":DEVICE_0_UUID,"token":DEVICE_0_TOKEN,"emitterUuid": DEVICE_0_UUID, "subscriberUuid": DEVICE_0_UUID, "type":"message.received"});
      // Send data from A to B:  OK

      // ---------NOT WORK--------
      // Scenario: Device B allows A to send a message, Device C is subscribed to B
      //nodes.updateV2_PUT({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"data":{"$set":{"meshblu":{"version" : "2.0.0","whitelists": {"message": {"from": [{"uuid": DEVICE_A_UUID}], "received":[{"uuid": DEVICE_A_UUID}]} }}}}});
      //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": DEVICE_C_UUID, "type":"message.received"});
      // Send data from A to B:  OK  Send data from B to C: NOT OK
      // 
      // Scenario: Device B allows A to send a message, Device D is subscribed to A
      //nodes.updateV2_PUT({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"data":{"$set":{"meshblu":{"version" : "2.0.0","whitelists": {"message": {"sent":[{"uuid": DEVICE_D_UUID}]  } }}}}});
      //nodes.subscription({"uuid":DEVICE_D_UUID,"token":DEVICE_D_UUID,"emitterUuid": DEVICE_A_UUID, "subscriberUuid": DEVICE_D_UUID, "type":"message.sent"});
      //nodes.subscription({"uuid":DEVICE_D_UUID,"token":DEVICE_D_TOKEN,"emitterUuid": DEVICE_D_UUID, "subscriberUuid": DEVICE_D_UUID, "type":"message.received"});
      // Send data from A to B:  OK  Send data from B to C: NOT OK send data from A to D NOT OK
      // ---------------------------

    // Send Broadcast Message
      //
      // Scenario: Device A broadcasts a message, Device B is subscribed to A
      //nodes.updateV2_PUT({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"data":{"$set":{"meshblu":{"version" : "2.0.0","whitelists": {"broadcast": {"received":[{"uuid": DEVICE_B_UUID}]  } }}}}});
      //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_A_UUID, "subscriberUuid": DEVICE_B_UUID, "type":"broadcast.sent"});
      //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": DEVICE_B_UUID, "type":"broadcast.received"});

  // 
  // nodes.messages({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN, "message":{
  //       "devices":[DEVICE_B_UUID],
  //       "topic":"SENT FROM DEVICE_A_UUID",
  //       "time": Date.now(),
  //       "payload":{
  //         "online":true
  //       }
  //     }});
  //nodes.messages({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN, "message":{"devices": "66ff0cab-af41-4f66-a136-0f4591e939fd", "payload": {"yellow":"off"}}});
  //nodes.messages({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN, "message":{"devices": [DEVICE_A_UUID], "payload": {"yellow":"no"}}});
  // SUBSCRIBE:
  //nodes.subscriptionsList({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN});
  //
  //NODE A->B: '{"$set": {"pigeonCount": 2}}'
  //ENABLE TO RECEVEID A DIRECT MESSAGE FROM ANOTHER NODE (EXAMPLE: MSG DA B AD A THEN ENABLE WITH UPDATE ON A TO RECEIVE DIRECT MESSAGE FROM B)
  //nodes.updateV2_PUT({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"data":{"$set":{"meshblu":{"version" : "2.0.0","whitelists": {"message": {"from": [{"uuid": DEVICE_B_UUID}]}}}}}});
  //ENABLE TO RECEVEID A MESSAGE FROM ANOTHER NODE
  //nodes.updateV2_PUT({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"data":{"$set":{"meshblu":{"version" : "2.0.0","whitelists": {"message": {"sent": [{"uuid": DEVICE_C_UUID}]}}}}}});
  
  //nodes.updateV2_PATCH({"uuid":DEVICE_A_UUID,"token":DEVICE_A_TOKEN,"data":{"whitelists": {"message": {"sent": [{"uuid": DEVICE_B_UUID}] } } }});

  // QUESTA SUBSCRIBE NON FUNZIONA  SENZA FARE UN UPDATE SUL DEVICE VEDI UPDATE SOPRA
  //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": DEVICE_C_UUID, "type":"message.sent"});
  //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_C_UUID, "subscriberUuid": DEVICE_C_UUID, "type":"message.received"});
  //NODE B ->A:
  // QUESTA SUBSCRIBE FUNZIONA ANCHE SENZA FARE NESSUN UPDATE SUL DEVICE PERCHè è L'EMITTER è UGUALE AL SUBSCRIBER
  //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": DEVICE_B_UUID, "type":"message.received"});
  //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_A_UUID, "subscriberUuid": DEVICE_B_UUID, "type":"message.sent"});
  //NODE B->C
  //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_C_UUID, "subscriberUuid": , "type":"message.received"});
  //nodes.subscription({"uuid":DEVICE_B_UUID,"token":DEVICE_B_TOKEN,"emitterUuid": DEVICE_C_UUID, "subscriberUuid": , "type":"message.sent"});
  //NODE C->B
  //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": , "type":"message.sent"});
  //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_C_UUID, "subscriberUuid": , "type":"message.sent"});

  //nodes.subscription({"uuid":DEVICE_C_UUID,"token":DEVICE_C_TOKEN,"emitterUuid": DEVICE_B_UUID, "subscriberUuid": , "type":"message.sent"});
  //
  //DELETE SUBSCRIPTION 
  //nodes.deleteSubscription({"uuid":DEVICE_0_UUID,"token":DEVICE_0_TOKEN,"emitterUuid": DEVICE_0_UUID, "subscriberUuid": DEVICE_0_UUID, "type":"message.received"});


