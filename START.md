START:
------
npm run start:dev


DOCKER LOCALHOST:
-----------------

    1) NETWORK:
    docker network create gatenode

    2) MONGODB:
    docker run --name mongodb --network gatenode -p 27017:27017 -d mongo:3.6.1

    3) CORE
    docker build -t gatenode_core .

    docker run  --rm -it --env-file env_file --name gatenode_core -v $(pwd)/env_file:/app/env_file --network gatenode -p 4100:4100 -p 3200:3200 gatenode_core  

    --network=host core // SERVE PER RENDERE VISIBILE MONGODB

    IMPORTANT: 
        - ALL'INTERNO DEL CORE INSERIRE IL NOME DEL CONTAINER DI MONGODB COME HOST TIPO E NON IP:

            mongodb://mongodb:27127/gatenode 

        - LANCIARE OGNI CONTAINER CON IL NOME DELLA RETE --network gatenode
        - INSERIRE NEL RUN SEMPRE IL NOME DEL CONTAINER --name gatenode_core


DOCKER REMOTE:
---------------
docker build -t   10.147.18.139:5000/gatenode_core .
docker push  10.147.18.139:5000/gatenode_core 

docker run  --rm -it --env-file env_file --name gatenode_core -v $(pwd)/env_file:/app/env_file --network gatenode -p 4100:4100 -p 3200:3200 10.147.18.139:5000/gatenode_core

docker tag gatenode_core  10.147.18.139:5000/gatenode_core

DOCKER BUILDX FOR DIFFERENT ARCHITECTURES:
------------------------------------------ 
Enable buildx from GUI otherwise from shell with command:


docker buildx build --platform linux/amd64,linux/arm64,linux/386,linux/arm/v6,linux/arm/v7 --push -t middlenode/gatenode_core:latest .

List of all platform: 
linux/amd64, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/arm/v6, linux/arm/v7

# ARM64
docker build -t 10.147.18.139:5000/gatenode_core:manifest-arm64 --build-arg ARCH=arm64/ .
docker push 10.147.18.139:5000/gatenode_core:manifest-arm64

docker manifest create \
10.147.18.139:5000/gatenode_core:manifest-latest \
--amend 10.147.18.139:5000/gatenode_core:manifest-arm64 \


docker buildx build --platform linux/arm64 -t 10.147.18.139:5000/gatenode_core:latest . --push


DOCKER-COMPOSE:
---------------
Quando si usa docker-compose bisogna eliminare il file env-file dal folder 
/server perchè diversamente non viene eseguito il mapping del volume
su questo file. Il file deve essere eliminato solo quando si crea 
l'immagine gate-node_core.
 