const logger = require('./app/plugins/logger');
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
global.app = express();

// READ do_not_touch_file: 
require('dotenv').config({ path: 'do_not_touch_file' });
// LOAD CONTROLLER//const ip = require("./app/controllers/ip.controller.js");
const settings = require("./app/config/settings.config.js");
const nodes = require("./app/controllers/node.controller.js");
const middlenode = require("./app/controllers/middlenode.controller.js");
const gatenode= require("./app/controllers/gatenode.controller.js");

// SWAGGER OPTIONS
const options_public = require("./app/swagger/option.public.js");
const options_private = require("./app/swagger/option.private.js");
const options_sauron = require("./app/swagger/option.sauron.js");


global.gatenode;

// TEST API:
const test = require("./test/api_nodes.js");

//ip.check();
gatenode.connect();

if(process.env.GATE_NODE_INIT==='false'){
  const interval = setInterval(() => {
    logger.info("NOT ABLE TO CONNECT TO MIDDLE-NODE");
    if(process.env.GATE_NODE_INIT==='true'){
      middlenode.create();
      logger.info("CONNECT TO MIDDLE-NODE");
      clearInterval(interval);
    }
  }, 1000)
}
if(process.env.GATE_NODE_INIT==='true'){

setTimeout(function(){
  middlenode.create(); 
},10000)
   
}

// API SWAGGER DOCUMENTATION 
const swaggerJsdoc = require('swagger-jsdoc')  
const swaggerUi = require('swagger-ui-express')  
const specs_public = swaggerJsdoc(options_public);
const specs_private = swaggerJsdoc(options_private);
const specs_sauron = swaggerJsdoc(options_sauron);

app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs_public,{ 
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }' 
  })
);
app.use(
  "/api-docs-private",
  swaggerUi.serve,
  swaggerUi.setup(specs_private,{ 
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }' 
  })
);
app.use(
  "/api-docs-sauron/v1",
  swaggerUi.serve,
  swaggerUi.setup(specs_sauron,{ 
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }' 
  })
);
// END SWAGGER API DOCUMENTATION 

// CORS
var corsOptions = {
  origin: [
  'http://'+process.env.GATENODE_GUI_HOSTNAME+':'+process.env.GATENODE_GUI_PORT,
  'http://localhost'+':'+process.env.GATENODE_GUI_PORT
  ],
  optionsSuccessStatus: 200,
  credentials: true
}

// var allowlist = ['http://localhost:4200']
// var corsOptionsDelegate = function (req, callback) {
//   var corsOptions;
//   if (allowlist.indexOf(req.header('Origin')) !== -1) {
//     corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
//   } else {
//     corsOptions = { origin: false } // disable CORS for this request
//   }
//   callback(null, corsOptions) // callback expects two parameters: error and options
// }
app.use(cors(corsOptions));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", req.header('Origin'));
  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});


app.use(bodyParser.text({ type: "*/*" }));
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// DB MODELS
const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    logger.info("Connected to the database!");
  })
  .catch(err => {
    logger.info("Cannot connect to the database!", err);
    process.exit();
  });

// Info route api server
app.get("/", (req, res) => {
  res.json({ message: 'Gate-Node API server. API documentation running on link: http://'+process.env.GATENODE_CORE_HOSTNAME+':'+process.env.GATENODE_CORE_PORT+'/api-docs' });
});

require("./app/routes/settings.routes")(app);
require("./app/routes/node.routes")(app);
require("./app/routes/device.routes")(app);
require("./app/routes/sse.routes")(app);

// SAURON ROUTES
require("./app/routes/algorithm.routes")(app);
require("./app/routes/source.routes")(app);
require("./app/routes/stream.routes")(app);
require("./app/routes/system.routes")(app);
// END SAURON ROUTES

// set port, listen for requests
const PORT = process.env.GATENODE_CORE_PORT || 4100;
app.listen(PORT, () => {
  logger.info(`Server is running on port ${PORT}.`);
  nodes.start();

  // NODE INIT
  // if(process.env.GATE_NODE_INIT!=='true'){
  //   logger.info(process.env.GATE_NODE_INIT);
  //   nodes.createInit();
  // };

});


