# base image
FROM node:10.15.3-alpine

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install libzmq and development headers
RUN apk add --no-cache make gcc g++ python zeromq-dev

# Tell zeromq to use shared library
ENV npm_config_zmq_external="true"

# Bundle app source
COPY . .

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

CMD [ "node", "index.js" ]

EXPOSE 4100
EXPOSE 3200


